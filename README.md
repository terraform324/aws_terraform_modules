# API modules

This is a collection of modules that can be used in building AWS resources.

<!-- BEGIN_TF_DOCS -->
## modules

This directory contains the modules.

## tutorial_examples

This contains examples of resources built with the modules. Each tutorial
is based on a tutorial (such as an AWS API Gateway tutorial) and should
include a README.md file with a link to the tutorial online.

