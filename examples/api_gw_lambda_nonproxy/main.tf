# Configure and downloading plugins for aws
provider "aws" {
  region = "us-east-2"
}

terraform {
  backend "s3" {
    key            = "aws_modules/tutorial_examples/api_gw_lambda_nonproxy/terraform.tfstate"
  }
}

module "api_gateway" {
  source  = "SPHTech-Platform/apigw/aws"
  version = "0.3.1"
  name = "LambdaNonProxy"
  stage = "test"
  body_template = file("templates/spec.yml")
  log_retention_in_days = 0
}


module "lambda_function" {
  source = "terraform-aws-modules/lambda/aws"
  version = "4.9.0"

  function_name = "GetStartedLambdaIntegration"
  description   = "My awesome lambda function"
  handler       = "index.handler"
  runtime       = "nodejs14.x"
  lambda_role   = "arn:aws:iam::583087165181:role/service-role/GetStartedLambdaIntegrationRole"
  role_name = "GetStartedLambdaIntegrationRole"
  role_path = "/service-role/"
  policy_path = "/service-role/"
  role_force_detach_policies = false
  publish = true
  allowed_triggers = {
    APIGatewayTest = {
      service    = "apigateway"
      source_arn = "${module.api_gateway.aws_api_gateway_stage_execution_arn}/*/*"
    }
  }
  source_path = "src/index.mjs"

  tags = {
    Name = "GetStartedLambdaIntegration"
  }
}
