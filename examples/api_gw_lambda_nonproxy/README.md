# API Gateway Lambda Nonproxy

This is a Terraform example of building a REST API with API Gateway
and a Lambda function. It is based on this AWS tutorial:

https://docs.aws.amazon.com/apigateway/latest/developerguide/getting-started-lambda-non-proxy-integration.html

## To initialize

Copy backend.hcl.example to backend.hcl and update it with values for the S3 bucket, AWS region and terraform lock table.

```
terraform init -backend-config=backend.hcl
```

## Registry modules used

For the API gateway:
https://github.com/SPHTech-Platform/terraform-aws-apigw

For the Lambda function:
https://github.com/terraform-aws-modules/terraform-aws-lambda

