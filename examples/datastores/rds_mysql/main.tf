# Configure and downloading plugins for aws
provider "aws" {
  region = "us-east-2"
}

terraform {
  backend "s3" {
    key            = "aws_terraform_modules/examples/datastores/rds_mysql/terraform.tfstate"
  }
}

locals {
  name   = "slotracker"
  region = "us-east-2"

  vpc_cidr = "172.31.0.0/16"

  tags = {
    Name       = local.name
    Repository = "https://github.com/terraform-aws-modules/terraform-aws-rds"
  }
}

module "rds_mysql" {
  source = "terraform-aws-modules/rds/aws"
  version = "5.4.1"

  identifier = local.name

  # All available versions: http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_MySQL.html#MySQL.Concepts.VersionMgmt
  engine               = "mariadb"
  engine_version       = "10.6.10"
  create_db_parameter_group = false
  create_db_option_group = false
  instance_class       = "db.t3.medium"
  create_random_password = false
  password             = var.password

  allocated_storage     = 200
  max_allocated_storage = 1000

  username = "admin"
  port     = 3306

  multi_az               = false
  # db_subnet_group_name   = module.vpc.database_subnet_group
  # vpc_security_group_ids = [module.security_group.security_group_id]
  maintenance_window              = "sun:08:48-sun:09:18"
  backup_window                   = "06:30-07:00"
  enabled_cloudwatch_logs_exports = [
    "error",
    "slowquery"
  ]
  create_cloudwatch_log_group     = true
  cloudwatch_log_group_retention_in_days = 0

  backup_retention_period = 7
  skip_final_snapshot     = true
  copy_tags_to_snapshot                 = true
  deletion_protection     = false

  performance_insights_enabled          = true
  performance_insights_retention_period = 7
  publicly_accessible                   = true

  tags = local.tags
  iam_database_authentication_enabled   = true
}

################################################################################
# Supporting Resources
################################################################################

# module "vpc" {
#   source  = "terraform-aws-modules/vpc/aws"
#   version = "~> 3.0"

#   name = "aws-controltower-VPC"
#   cidr = local.vpc_cidr
#   enable_dns_hostnames = true
#   create_database_subnet_group = false

#   tags = local.tags
# }

# module "security_group" {
#   source  = "terraform-aws-modules/security-group/aws"
#   version = "~> 4.0"
#   name        = "MariaDB"
#   use_name_prefix = false
#   description = "Created by RDS management console"
#   vpc_id      = module.vpc.vpc_id

#   # ingress
#   ingress_with_cidr_blocks = [
#     {
#       from_port   = 0
#       to_port     = 65535
#       protocol    = "tcp"
#       description = "VPC"
#       cidr_blocks = module.vpc.vpc_cidr_block
#     },
#     {
#       from_port   = 3306
#       to_port     = 3306
#       protocol    = "tcp"
#       description = "Internal VPN"
#       cidr_blocks = "209.134.145.19/32"
#     },
#   ]
#   tags = local.tags
# }
