# RDS MySQL
An example of using the terraform-aws-rds module repository to manage a MariaDB database.

## Repository
https://github.com/terraform-aws-modules/terraform-aws-rds

## To initialize

Copy backend.hcl.example to backend.hcl and update it with values for the S3 bucket, AWS region and terraform lock table.

```
terraform init -backend-config=backend.hcl
```

## Terraform imports
Commands like the following could be used to import the resources built in this example:

```
terraform import module.rds_mysql.module.db_instance.aws_db_instance.this[0] slotracker
terraform import module.security_group.aws_security_group.this[0] sg-029b6a357ea381179
terraform import module.security_group.aws_security_group_rule.ingress_with_cidr_blocks[0] sg-029b6a357ea381179_ingress_tcp_0_65535_172.31.0.0/16
terraform import module.security_group.aws_security_group_rule.ingress_with_cidr_blocks[1] sg-029b6a357ea381179_ingress_tcp_3306_3306_209.134.145.19/32
terraform import module.vpc.aws_vpc.this[0] vpc-0be8d6920e393d742
terraform import module.rds_mysql.module.db_instance.aws_cloudwatch_log_group.this[\"error\"] /aws/rds/instance/slotracker/error
terraform import module.rds_mysql.module.db_instance.aws_cloudwatch_log_group.this[\"slowquery\"] /aws/rds/instance/slotracker/slowquery
terraform import module.rds_mysql.module.db_option_group.aws_db_option_group.this[0] default:mariadb-10-6
```